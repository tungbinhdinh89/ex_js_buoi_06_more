// Viết chương trình có một ô input, một button. Khi click vào button thì in ra các số nguyên tố từ 1 tới giá trị của ô input

// Method one
function isPrimeNumber(number) {
  // Biến cờ hiệu
  var checkPrimeNumber = true;

  // Nếu n bé hơn 2 tức là không phải số nguyên tố
  if (number < 2) {
    checkPrimeNumber = false;
  } else if (number == 2) {
    checkPrimeNumber = true;
  } else if (number % 2 == 0) {
    checkPrimeNumber = false;
  } else {
    // lặp từ 3 tới number-1 với bước numberhảy là 2 (i+=2)
    for (var i = 3; i <= Math.sqrt(number); i += 2) {
      if (number % i == 0) {
        checkPrimeNumber = false;
        break;
      }
    }
  }

  return checkPrimeNumber;
}
isPrimeNumber(20);
/// Method two

function kiemTraPrimeNumber(n) {
  var isprime = n == 1 ? false : true; //because 1 is not prime
  for (var i = 2; i < n; i++) {
    n % i == 0 ? (isprime *= false) : (isprime *= true);
  }
  return isprime;
}
//   console.log(`${n} is ${isprime ? 'prime' : 'not prime'} number`);

kiemTraPrimeNumber(20);

// var primeNumber = isPrimeNumber(soNhap);

// Hàm in ra các số nguyên tố từ 1 tới n
function inSoNguyenTo() {
  // Lấy number

  var soNhap = document.querySelector('#nhap_so').value * 1;
  console.log('soNhap: ', soNhap);

  // Lặp để in kết quả
  var isNguyenTo = '';
  for (var i = 1; i <= soNhap; i++) {
    // Nếu là số nguyên tố thì in ra
    if (kiemTraPrimeNumber(i)) {
      isNguyenTo += i + ', ';
    }
  }
  document.getElementById('result').innerHTML = isNguyenTo;
}
